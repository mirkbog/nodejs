// requires...
const fs = require("fs").promises;
const path = require("path");
const url = require("url");

// constants...
const filesFolder = path.join(__dirname, "files");
const EXTENSIONS = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];

async function createFile(req, res, next) {
  // Your code to create the file.
  const { filename, content } = req.body;
  if (!content || !filename) {
    res.status(400).send({ message: "Invalid request, missing paramater" });
  }
  const normalizedPath = path.join(filesFolder, filename);
  const isAcceptableExt = EXTENSIONS.includes(path.extname(filename));

  let { query } = url.parse(req.url, true);
  if (query.password) {
    const allFiles = await fs.readdir(__dirname);
    if (allFiles.includes("passwords.json")) {
      let passwordsPath = path.join(__dirname, "passwords.json");
      const passwordContents = await fs
        .readFile(passwordsPath, "utf8")
        .then(JSON.parse);

      const newContent = { ...passwordContents, [filename]: query.password };

      await fs.writeFile(passwordsPath, JSON.stringify(newContent), "utf8");
    } else {
      const passwordFile = path.join(__dirname, "passwords.json");
      await fs.writeFile(
        passwordFile,
        JSON.stringify({ [filename]: query.password }),
        "utf8"
      );
    }
  }

  if (isAcceptableExt) {
    await fs.writeFile(normalizedPath, content, "utf8");
    res.status(200).send({ message: "File created successfully" });
  } else {
    res.status(400).send({ message: "Invalid request, missing paramater" });
  }
}

async function getFiles(req, res, next) {
  // Your code to get all files.

  const filesFolderContents = await fs.readdir(filesFolder);

  res.status(200).send({
    message: "Success",
    files: filesFolderContents,
  });
}

async function getFile(req, res, next) {
  // Your code to get all files.
  let { query } = url.parse(req.url, true);
  const { filename } = req.params;
  const filesFolderContents = await fs.readdir(filesFolder);

  if (
    filesFolderContents.length === 0 ||
    !filesFolderContents.includes(filename)
  ) {
    res.status(400).send({
      message: "Not found",
    });
  } else {
    if(query.password){
    let passwordsPath = path.join(__dirname, "passwords.json");
    const passwordContents = await fs
      .readFile(passwordsPath, "utf8")
      .then(JSON.parse);

    if (passwordContents[`${filename}`]) {
      if (passwordContents[`${filename}`] !== query.password) {
        res.status(400).send({
          message: "Wrong password",
        });
      }
    }
    }

    const normalizedPath = path.join(filesFolder, filename);

    const fileNameContents = await fs.readFile(normalizedPath, "utf8");
    const fileNameExt = path.extname(normalizedPath);
    const { birthtime } = await fs.stat(normalizedPath);

    let extensionWithoutDot = fileNameExt.split(".")[1];

    if (filesFolderContents.includes(filename)) {
      res.status(200).send({
        message: "Success",
        filename,
        content: fileNameContents,
        // extension: fileNameExt,
        extension: extensionWithoutDot,
        uploadedDate: birthtime,
      });
    }
  }
}

// Other functions - editFile, deleteFile

async function deleteFile(req, res, next) {
  const { filename } = req.params;
  const normalizedPath = path.join(filesFolder, filename);
  await fs.unlink(normalizedPath);
  res.status(200).send({ message: "File has been successfully deleted" });
}

async function editFile(req, res, next) {
  const { filename } = req.params;
  const { content } = req.body;
  const filesFolderContents = await fs.readdir(filesFolder);
  if (filesFolderContents.includes(filename)) {
    const normalizedPath = path.join(filesFolder, filename);
    await fs.writeFile(normalizedPath, content, "utf8");
    res.status(200).send({ message: `File ${filename} has been updated` });
  } else {
    res.status(400).send({ message: "There is no file with such name" });
  }
}

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile,
};
